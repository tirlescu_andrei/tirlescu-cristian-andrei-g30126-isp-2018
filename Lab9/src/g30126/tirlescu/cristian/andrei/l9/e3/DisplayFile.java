package g30126.tirlescu.cristian.andrei.l9.e3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DisplayFile extends JFrame {

    JLabel fileName;
    JTextField name;
    JButton openFile;
    JTextArea display;

    DisplayFile(){
        setTitle("DisplayFile");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600,700);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width = 100; int height = 20;

        fileName = new JLabel("FileName");
        fileName.setBounds(10,50,width,height);

        name = new JTextField();
        name.setBounds(70,50,width,height);

        openFile = new JButton("OpenFile");
        openFile.setBounds(10,100,width,height);
        openFile.addActionListener(new TratareButon());

        display = new JTextArea();
        display.setBounds(10,150,400,400);

        add(fileName); add(name); add(openFile); add(display);
    }

    public static void main(String[] args) {
        new DisplayFile();
    }

    public String readFromFile(){
        String s,s2 = new String();
        try(BufferedReader in = new BufferedReader(new FileReader(DisplayFile.this.name.getText()))){
            while((s = in.readLine()) != null)
                s2 += s +"\n";
        }catch (IOException e){
            System.out.println("Invalid file name!");
        }
        return s2;
    }

    class TratareButon implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e){
            String nameText = DisplayFile.this.name.getText();
            name.setText(nameText);
            if(nameText.equals(nameText)){
                DisplayFile.this.display.append(readFromFile());
            }
        }
    }

}
