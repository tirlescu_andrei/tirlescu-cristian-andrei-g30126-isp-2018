package g30126.tirlescu.cristian.andrei.l9.e2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CounterText extends JFrame{

    JLabel counter;
    JTextField tCounter;
    JButton increment;

    CounterText(){
        setTitle("Counter window");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width=100;int height = 20;

        counter = new JLabel("Counter ");
        counter.setBounds(10, 50, width, height);

        tCounter = new JTextField();
        tCounter.setText(0+"");
        tCounter.setBounds(70,50,width, height);

        increment = new JButton("Increment");
        increment.setBounds(10,100,width, height);
        increment.addActionListener(new TratareButon());

        add(counter);add(tCounter);add(increment);
    }

    public static void main(String[] args) {
        new CounterText();
    }

    class TratareButon implements ActionListener{
        private int counter = 0;
        @Override
        public void actionPerformed(ActionEvent e) {
            counter++;
            tCounter.setText(counter+"");
        }
    }
}