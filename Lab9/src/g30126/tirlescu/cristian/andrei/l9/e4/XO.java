package g30126.tirlescu.cristian.andrei.l9.e4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class XO extends JFrame {
    static int counter = 9;
    static JButton[][] butoane = new JButton[3][3];

    XO(){
        setTitle("XO-Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600,600);
        setVisible(true);
    }

    public void init(){
        new TratareButon();
        this.setLayout(null);
        int width = 70; int height = 70;

        for(int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                butoane[i][j] = new JButton("");
                butoane[i][j].setBounds(width*i,height*j, 70,70);
                butoane[i][j].setName(i+""+j);
                add(butoane[i][j]);
                butoane[i][j].addActionListener(new TratareButon());
            }
        }
    }

    public boolean chefRowCol(String s1, String s2, String s3){
        return ( (!s1.equals("")) && (s1.equals(s2)) && (s2.equals(s3)) );
    }

    public boolean checkDiagonalsForWin(){
        return ( (chefRowCol(butoane[0][0].getText(),butoane[1][1].getText(),butoane[2][2].getText())) ||
                (chefRowCol(butoane[0][2].getText(),butoane[1][1].getText(),butoane[2][0].getText())));
    }

    public boolean checkRowsForWin(){
        for (int i=0; i<3; i++){
            if(chefRowCol(butoane[i][0].getText(),butoane[i][1].getText(),butoane[i][2].getText()))
                return true;
        }
        return false;
    }

    public boolean checkColumnsForWin(){
        for (int i=0; i<3; i++){
            if(chefRowCol(butoane[0][i].getText(),butoane[1][i].getText(),butoane[2][i].getText()))
                return true;
        }
        return false;
    }

    class TratareButon implements ActionListener{
        private String name = null;
        @Override
        public void actionPerformed(ActionEvent e) {
            name = ((JButton)e.getSource()).getName();
            int i = Integer.parseInt(name.charAt(0)+"");
            int j = Integer.parseInt(name.charAt(1)+"");
            if(counter % 2 == 0){
                butoane[i][j].setText("O");
                butoane[i][j].setEnabled(false);
            }
            if(counter % 2 == 1){
                butoane[i][j].setText("X");
                butoane[i][j].setEnabled(false);
            }
            if(counter == 1){
                System.out.println("Out of move!");
                System.out.println("End of game!");
                return;
            }
            if(checkDiagonalsForWin()){
                System.out.println("The winner is: "+butoane[i][j].getText());
                System.out.println("End of the game!");
                return;
            }
            if(checkColumnsForWin()){
                System.out.println("The winner is: "+butoane[i][j].getText());
                System.out.println("End of the game!");
                return;
            }
            if(checkRowsForWin()){
                System.out.println("The winner is: "+butoane[i][j].getText());
                System.out.println("End of the game!");
                return;
            }
            System.out.println("Counter="+counter);
            counter --;
        }
    }
}
