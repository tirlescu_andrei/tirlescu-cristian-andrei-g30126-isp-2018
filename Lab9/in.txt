Hello, my name is Tirlescu Cristian Andrei and today I will
present to you some informations about Nanorobotisc. After
this presentation I hope that you will know much more about
the nanorobots.
So, let's get started!

The main topics are: What does nanotechnology actually mean?
Different types of Nanorobots and Applications and here I
will talk about different types of nanoengines like Smallest
engine ever created witch is an engine made with graphene, 
an ant-like nanoengine and Bacteria-powered robots.
And in the last I want to discuss about Key aPplications of
Nano and Micro-Machines.

First, I will start with a question witch is : What does Nano
technology actually mean? Can someone tell me?
Do you think that nanorobots look like this?
Can someone tell me what is the diameter of a red blood cell?
Answer : between 7,000-8,000 nanometers.
So, to understand better what is the difference between a
nanorobot and a microrobot let's start with the definition of
nanotechnology: ... So like you see, is a sciente witch is
applied at nanoscale level, witch means that this robot is
not a nanorobot. In the next slides I will prove that to you.
To understand better how tiny a nanometer is let's imaginate
an ant witch is a million times smaller than a real one.
A nanorobot, then, is a machine that can build and manipulate
things precisely at an atomic level. Imagine a robot that
can pluck, pick and place atoms like a kid plays with LEGO 
bricks, able to build anything from basic atomic building 
blocks (C, N, H, O, P, Fe, Ni, and so on).? 

Ok, let's see what is a nanorobot and how you can create them
Here, we can see the smallest engine ever created. This is an
engine mede from a elastice piece of graphine. The
atom is trapped in a cone of electromagnetic energy and
lasers are used to heat it up and cool it down.
Graphene is a two-dimensional sheet of carbon atoms that has 
exceptional mechanical strength. Inserting some chlorine and 
fluorine molecules into the graphene lattice and firing a laser 
at it causes the sheet to expand. Rapidly turning the laser on 
and off makes the graphene pump back and forth like the piston in 
an internal combustion engine.

Second engine witch has name ANT is created by adding a 
polymer called pNIPAM to gold nanoparticles. While heating
them with a blue-light laser,the particles are losing the water from 
polymer and agregate into a tight ball. By colling process
the particles will receive the water and this will produce a 
suddlen explosion. This engine can produce a force per 
unit-weigth 100 times higher than any motor or muscle.
What has in common this engine with the first one?
Why they use gold? I think that the gold does not interact with the
water like other elemets. It cannot oxide.

The third engine is called Bacteria-powered robot because it is
created from a bacteria named,flagellated Bacteria Serratia 
marcescens and an electric field.
In the picture we can see that when we apply an electric field
the robots is moving because the bacteria have a natural nega
tive charge and it is attracted by the positive charge. Also,
by using an electric field you can detecte obstacles in the
environment. Why the scientists chosen this bacteria? Simply,
because it has a flagella witch reduce friction so the move-
ment can be smoother. 
In the second picture we can see the bacteria and the form of it.

