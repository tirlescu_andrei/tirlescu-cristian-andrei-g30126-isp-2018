package g30126.tirlescu.cristian.andrei.l10.e4;

import java.util.Random;

public class Main {

    private static int[][] board = new int[100][100];
    private static boolean verify = true;
    private static Robot[] robots = new Robot[10];
    private static SetPosition[] setPositions = new SetPosition[10];
    private static GetPosition[] getPositions = new GetPosition[10];

    public static void main(String[] args) {
//        Robot robot = new Robot();
//        SetPosition sp = new SetPosition(robot);
//        GetPosition gp = new GetPosition(robot);
//
//        sp.start();
//        gp.start();
        initRobots();
        setRobots();
        getRobots();
    }

    static class SetPosition extends Thread {
    Robot r;
    Random random = new Random();

    public SetPosition(Robot r) {
        this.r = r;
    }

    int x = random.nextInt(100);
    int y = random.nextInt(100);
    int xNew = x;
    int yNew = y;

    public void run() {

            while(true){
                xNew = xNew + random.nextInt(3) - 1;
                yNew = yNew + random.nextInt(3) - 1;

                synchronized (r){
                    while(verify){
                        r.setXY(x, y);
                        try{
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        board[r.getX()][r.getY()] = 1;
                        verify = false;
                    }
                    if((xNew>=0) && (yNew>=0) && (xNew<=100) && (yNew<=100)){
                        board[r.getX()][r.getY()] = 0;
                        r.setXY(xNew,yNew);
                        try{
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            System.out.println(this.getName()+" stopped!");
                        }
                        try{
                            board[r.getX()][r.getY()] = 1;
                            Thread.sleep(50);
                        } catch (ArrayIndexOutOfBoundsException e){
                            xNew = xNew + random.nextInt(3) - 1;
                            yNew = yNew + random.nextInt(3) - 1;
                        } catch (InterruptedException e) {
                            System.out.println("Thread is interrupted");
                        }

                    }
                }
                System.out.println("Am scris: [" + (r.getX()+1) + "," + (r.getY()+1) + "]");
            }
        }
    }

    static class GetPosition extends Thread {
        Robot r;
        private int x,y;

        public GetPosition(Robot r){this.r = r;}

        public void run(){
            while (true){
                synchronized (r){
                    x = r.getX();
                    try{
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    y = r.getY();
                    for(int i=0; i<robots.length-1; i++){
                        int a = robots[i].getX();
                        int b = robots[i].getY();
                        int A = robots[i+1].getX();
                        int B = robots[i+1].getY();
                        try {
                            Thread.sleep(50);
                            if(board[a][b] == board[A][B]){
                                setPositions[i].interrupt();
                                setPositions[i+1].interrupt();
                                board[a][b] = board[A][B] = 0;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ArrayIndexOutOfBoundsException e){
                            e.getMessage();
                        }

                    }
                }
                System.out.println("Am citit: ["+(x+1)+","+(y+1)+"]");
//                afisare();
            }
        }


//        public synchronized void afisare(){
//            for(int i=0; i<board.length; i++)
//            {
//                for(int j=0; j<board.length; j++){
//                    System.out.print(board[i][j]);
//                }
//                System.out.println();
//            }
//        }
    }

    static void initRobots(){
        for(int i=0; i<robots.length; i++){
            robots[i] = new Robot();
        }
    }

    static void setRobots(){
        for(int i=0; i<robots.length; i++){
            setPositions[i] = new SetPosition(robots[i]);
            setPositions[i].start();
        }
    }

    static void getRobots(){
        for(int i=0; i<robots.length; i++){
            getPositions[i] = new GetPosition(robots[i]);
            getPositions[i].start();
        }
    }
}

class Robot {
    private int x;
    private int y;

    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}