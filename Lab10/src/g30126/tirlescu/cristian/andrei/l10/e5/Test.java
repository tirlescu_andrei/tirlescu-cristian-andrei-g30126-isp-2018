package g30126.tirlescu.cristian.andrei.l10.e5;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        Producer producer = new Producer(buffer);
        Consumer c1 = new Consumer(buffer);
        Consumer c2 = new Consumer(buffer);

        producer.start();
        c1.start();
        c2.start();
    }
}

class Producer implements Runnable {
    Buffer buffer;
    Thread thread;

    public Producer(Buffer buffer){this.buffer = buffer;}

    public void start(){
        if(thread == null){
            thread = new Thread(this);
            thread.start();
        }
    }

    public void run(){
        while(true){
            buffer.push(Math.random());
            System.out.println("Am scris.");
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class Consumer extends Thread{
    private Buffer buffer;
    Consumer (Buffer buffer){this.buffer = buffer;}

    public void run(){
        while(true){
            System.out.println("Am citit : "+this+" >> "+buffer.get());
        }
    }
}

class Buffer {
    ArrayList content = new ArrayList();

    synchronized void push(double d){
        content.add(new Double(d));
        notify();
    }

    synchronized double get(){
        double d = -1;
        try{
            while(content.size()==0) wait();
            d = ((Double)content.get(0)).doubleValue();
            content.remove(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return d;
    }
}