package g30126.tirlescu.cristian.andrei.l10.e1;

public class CounterRunnable implements Runnable{

    @Override
    public void run() {
        Thread t = Thread.currentThread();
        for(int i=0; i<20; i++){
            System.out.println("Thread name:"+t.getName()+ " i= "+i);
            try{
                Thread.sleep(1000);
                if( i == 3)
                    return;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Ending of the counting");
    }

    public static void main(String[] args) {
        CounterRunnable c1 = new CounterRunnable();
        CounterRunnable c2 = new CounterRunnable();

        Thread t1 = new Thread(c1,"Counter1");
        Thread t2 = new Thread(c1,"Counter2");

        t1.start();
        t2.start();
    }
}