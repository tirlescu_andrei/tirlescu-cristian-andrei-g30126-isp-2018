package g30126.tirlescu.cristian.andrei.l10.e1;

public class Counter extends Thread{
    Counter(String name){
        super(name);
    }

    public void run(){
        for(int i=0; i<20; i++){
            System.out.println("Thread name:"+getName() + " i="+i);
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("Ending of execution!");
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("Counter1");
        Counter c2 = new Counter("Counter2");
        Counter c3 = new Counter("Counter3");

        c1.run();
        c2.run();
        c3.run();
    }
}

