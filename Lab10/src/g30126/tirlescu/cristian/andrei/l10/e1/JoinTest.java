package g30126.tirlescu.cristian.andrei.l10.e1;

//public class JoinTest extends Thread {
//    String n;
//    Thread t;
//    JoinTest(String n, Thread t){
//        this.n = n;
//        this.t = t;
//    }
//
//    public void run(){
//        System.out.println("Firul "+n+" a intrat in metoda run()");
//        try{
//            if(t!=null) t.join();
//            System.out.println("Firul "+n+" executa operatia.");
//            Thread.sleep(6000);
//            System.out.println("Firul "+n+" a terminat operatia.");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void main(String[] args) {
//        JoinTest w1 = new JoinTest("Proces1",null);
//        JoinTest w2 = new JoinTest("Proces2",w1);
//
//        w1.start();
//        w2.start();
//    }
//}

class TestJoinMethod1 extends Thread{
    public void run(){
        for(int i=1;i<=5;i++){
            try{
                Thread.sleep(500);
            }catch(Exception e){System.out.println(e);}
            System.out.println(i);
        }
    }
    public static void main(String args[]){
        TestJoinMethod1 t1=new TestJoinMethod1();
        TestJoinMethod1 t2=new TestJoinMethod1();
        TestJoinMethod1 t3=new TestJoinMethod1();
        t1.start();
        try{
            t1.join();
        }catch(Exception e){System.out.println(e);}

        t2.start();
        t3.start();
    }
}