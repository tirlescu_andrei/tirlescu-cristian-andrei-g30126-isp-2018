package g30126.tirlescu.cristian.andrei.l10.pregatire;

import java.util.Random;

public class Main {
    private static int[][] board = new int[5][5];

    public static void main(String[] args) throws InterruptedException {
        Robot r = new Robot();
        Robot r1 = new Robot();
        setPosition sp = new setPosition(r);
        getPosition gp = new getPosition(r);
        setPosition sp1 = new setPosition(r1);
        getPosition gp1 = new getPosition(r1);

        sp.start();
        gp.start();
        sp1.start();
        gp1.start();

//        if((gp.r.getX() == gp1.r.getX()) && ((gp.r.getY()) == gp1.r.getY())){
//            gp.thread.interrupt(); gp1.thread.interrupt(); sp1.thread.interrupt(); sp.thread.interrupt();
//            System.out.println(gp.thread.getName()+" INTERRUPTED!");
//        }

    }

    static class setPosition implements Runnable{
        Robot r;
        Thread thread;
        private boolean active = true;

        public setPosition(Robot r){
            this.r = r;
        }

        public void start(){
            if(thread == null){
                thread = new Thread(this);
                System.out.println("Sunt in start()!");
                System.out.println("x="+r.getX()+" y="+r.getY());
                board[r.getX()][r.getY()] = 1;
                System.out.println("============");
                thread.start();
            }
        }

        @Override
        public void run() {
            while(active){
                synchronized (r){
                    r.setXY();
                }
                try{
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(thread.getName()+"->> Am scris: [" + r.getX() + "," + r.getY() + "]");
            }
        }
    }


    static class getPosition implements Runnable{

        Robot r;
        Thread thread;
        private int x,y;
        private boolean active = true;

        public getPosition(Robot r){this.r = r;}

        public void start(){
            if(thread == null){
                thread = new Thread(this);
                thread.start();
            }
        }

        @Override
        public void run() {
            while (active){
                synchronized (r){
                    x = r.getX();
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    y = r.getY();
                }
                try{
                    board[x][y] = 1;
                } catch (ArrayIndexOutOfBoundsException e){
                    if(x == 5){
                        x -= 1;
                    } else if(y == 5){
                        y -= 1;
                    }
                }
                System.out.println(thread.getName()+"->> Am citit: ["+x+","+y+"]");
                afisare();
                try{
                    board[x][y] = 0;
                } catch (ArrayIndexOutOfBoundsException e){
                    if(x == -1){
                        x += 1;
                    } else if(y == -1){
                        y += 1;
                    }
                }
            }
        }

    private synchronized void afisare(){
        for (int[] aBoard : board) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(aBoard[j]);
            }
            System.out.println();
          }
        }
    }

    static class Robot{
        private int x;
        private int y;
        private Random random = new Random();

        public Robot(){
            this.x = random.nextInt(4);
            this.y = random.nextInt(4);
        }

        public void setXY(){
            this.x = x + random.nextInt(3)-1;
            this.y = y + random.nextInt(3)-1;
            if(x == -1 ){
                x += 1;
            }else if (x == 5){
                x -= 1;
            }
            else if(y == -1){
                y += 1;
            } else if (y == 5){
                y -= 1;
            }
        }
        public int getX() {
            return x;
        }
        public int getY() {
            return y;
        }
    }
}


