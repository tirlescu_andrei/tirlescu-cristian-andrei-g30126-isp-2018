package g30126.tirlescu.cristian.andrei.l10.pregatire;

import java.util.Random;

public class SM {
    public static void main(String[] args) {
        Punct p = new Punct();
        Set set = new Set(p);
        Get get = new Get(p);

        set.start();
        get.start();
    }
}

class Set implements Runnable{

    Punct p;
    Thread thread;
    Random random = new Random();

    public Set(Punct p){this.p = p;}

    public void start(){
        if(thread == null){
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {


        while(true){
            int x = (int)(Math.random()*10);
            int y = (int)(Math.random()*10);

            synchronized (p){
                p.setXY(x,y);
            }
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}

class Get implements Runnable{
    Punct p;
    Thread thread;
    private int x,y,suma;

    public Get(Punct p){
        this.p = p;
    }

    public void start(){
        if(thread == null){
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        while(true){
            synchronized (p){
                x = p.getX();
                y = p.getY();
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                suma += x+y;
            }

            System.out.println("x="+x+" y="+y);
            System.out.println("Suma = "+suma);
        }
    }
}

class Punct{
    int x,y;
    public void setXY(int a, int b){
        x = a; y = b;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}