package g30126.tirlescu.cristian.andrei.l10.e6;

import com.sun.xml.internal.ws.handler.HandlerException;

import javax.swing.*;

public class UI extends JFrame {
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JButton button1;
    private JButton button2;

    public UI() throws HandlerException{
        setSize(500,500);
        setTitle("Chronometer");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);

        jLabel1 = new JLabel("min");
        jLabel1.setBounds(100,100,50,50);
        jLabel2 = new JLabel("sec");
        jLabel2.setBounds(150,100,50,50);

        jTextField1 = new JTextField();
        jTextField1.setBounds(100,150,50,50);
        jTextField2 = new JTextField();
        jTextField2.setBounds(150,150,50,50);

        button1 = new JButton("STOP/START");
        button1.setBounds(100,200,150,50);
        button2 = new JButton("RESET");
        button2.setBounds(250,200,100,50);

        add(jLabel1); add(jLabel2); add(jTextField1); add(jTextField2); add(button1); add(button2);
    }

    public JTextField getjTextField1() {
        return jTextField1;
    }

    public JTextField getjTextField2() {
        return jTextField2;
    }

    public JButton getButton1() {
        return button1;
    }
    public JButton getButton2() {
        return button2;
    }
}
