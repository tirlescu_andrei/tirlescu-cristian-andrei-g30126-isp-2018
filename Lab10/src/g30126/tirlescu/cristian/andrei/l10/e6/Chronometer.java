package g30126.tirlescu.cristian.andrei.l10.e6;

import java.util.Observable;

public class Chronometer extends Observable implements Runnable {
    private Integer synchObj = new Integer(0);
    private int sec, min;
    private boolean active = true;
    private boolean reset = false;

    public Chronometer(){
        sec = 0; min = 0;
        new Thread(this).start();
    }

    public void run(){
        while(true){
            if(!active){
                synchronized (synchObj){
                    try{
                        synchObj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else{
                synchronized (this){
                    this.notify();
                }
            }
            if(reset){
                synchronized (this){
                    sec = 0;
                    min = 0;
                    reset = false;
                }
            }
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sec++;
            if(sec == 61){
                sec -= 61;
                ++min;
            }
            this.notifyObservers(sec);
            this.setChanged();
        }
    }

    public void changeState(){
        active = !active;
        if(active){
            synchronized (synchObj){
                synchObj.notify();
            }
        }
    }
    public void setReset(){
        reset = !reset;
    }
    public int getSec() {
        return sec;
    }
    public int getMin() {
        return min;
    }

}
