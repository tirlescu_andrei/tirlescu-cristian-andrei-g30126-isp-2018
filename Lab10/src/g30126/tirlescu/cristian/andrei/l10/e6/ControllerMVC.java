package g30126.tirlescu.cristian.andrei.l10.e6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class ControllerMVC implements Observer {
    private Chronometer chronometer;
    private UI ui;

    public ControllerMVC(Chronometer chronometer, UI ui){
        this.chronometer = chronometer;
        this.ui = ui;
        ui.getButton1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("STOP/START");
                chronometer.changeState();
            }
        });
        ui.getButton2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("RESET!");
                chronometer.setReset();
            }
        });
        chronometer.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        ui.getjTextField1().setText(chronometer.getMin()+"");
        System.out.println(chronometer.getSec());
        ui.getjTextField2().setText(arg.toString());
    }
}
