package g30126.tirlescu.cristian.andrei.l10.curs10;

import javax.swing.*;
import java.awt.*;

public class UI  extends JFrame {

    private JTextField jTextField = new JTextField();
    private JButton button = new JButton("OK");

    public UI() throws HeadlessException {
        setSize(400,400);
        setLayout(new FlowLayout());

        add(jTextField);
        add(button);
        setVisible(true);
    }

    public JTextField getjTextField() {
        return jTextField;
    }

    public JButton getButton() {
        return button;
    }
}
