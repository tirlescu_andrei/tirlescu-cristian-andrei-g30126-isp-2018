package g30126.tirlescu.cristian.andrei.l10.curs10;

import java.util.Observable;
import java.util.Random;

public class Senzor extends Observable implements Runnable{

    private Integer synchObj = new Integer(0);
    private int sec;

    boolean active = true;
    Senzor(){
        sec = 0;
        new Thread(this).start();
    }
    @Override
    public void run() {
        while(true){

            if(!active){
                synchronized (synchObj){
                    try {
                        synchObj.wait();
                    } catch (InterruptedException e) {
                        e.getMessage();
                    }
                }
            }else{
                synchronized (this){
                    this.notify();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Tick!");
            sec ++;
//            Random r = new Random();
//            int i = r.nextInt(100);

//            this.notifyObservers(i);
            this.notifyObservers(sec);
            this.setChanged();

        }
    }

    public void changeState() {
        active = !active;
        if(active){
            synchronized (synchObj){
                synchObj.notify();
            }
        }
    }
}