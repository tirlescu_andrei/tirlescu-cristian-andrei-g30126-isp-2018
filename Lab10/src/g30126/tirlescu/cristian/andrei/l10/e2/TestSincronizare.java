package g30126.tirlescu.cristian.andrei.l10.e2;

public class TestSincronizare {
    public static void main(String[] args) {
        Punct p = new Punct();
        FirstSet fs1 = new FirstSet(p);
        FirstGet fg1 = new FirstGet(p);

        fs1.start();
        fg1.start();
    }
}

class FirstGet extends Thread{
    Punct p;

    public FirstGet(Punct p){ this.p = p; }

    public void run(){
        int suma = 0;
        int i = 0;
        int a,b;
        while(++i<15){
            synchronized (p){
                a = p.getX();
                try{
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                 b = p.getY();
                }


            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}

class FirstSet extends Thread{
    Punct p ;
    public FirstSet(Punct p){ this.p = p;}

    public void run(){
        int i = 0;
        while(++i<15){
            int a = (int)Math.round(10*Math.random()+10);
            int b = (int)Math.round(10*Math.random()+10);

                synchronized (p){
            p.setXY(a,b);
            try{
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Am scris: ["+a+","+b+"]");
                }
        }
    }
}

class Punct{
    int x,y;
    public void setXY(int a, int b){
        x = a; y = b;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}