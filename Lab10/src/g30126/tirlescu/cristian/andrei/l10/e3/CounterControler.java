package g30126.tirlescu.cristian.andrei.l10.e3;

public class CounterControler extends Thread{
    FirstCounter fc;
    SecondCounter sc;

    public CounterControler(FirstCounter fc, SecondCounter sc) {
        this.fc = fc;
        this.sc = sc;
    }

    public void run(){
        try{
            if(fc.isOn()){
                fc.start();
                fc.join();
            }
            sc.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        FirstCounter fc = new FirstCounter(1,100);
        SecondCounter sc = new SecondCounter(101,200);
        CounterControler ctrl = new CounterControler(fc,sc);

        ctrl.start();
    }
}
