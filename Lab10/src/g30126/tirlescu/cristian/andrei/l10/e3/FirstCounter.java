package g30126.tirlescu.cristian.andrei.l10.e3;

public class FirstCounter extends Thread{
    private int valMin;
    private int valMax;
    private boolean isOn;

    public FirstCounter(int valMin,int valMax){
        this.valMin = valMin;
        this.valMax = valMax;
        isOn = true;
    }

    public void run(){
        for(int i=valMin; i<=valMax; i++){
            System.out.println("i= "+i);
            try{
                Thread.sleep(100);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        isOn = false;
        System.out.println("Ending of FirstCounter!");
    }

    public int getValMax() {
        return valMax;
    }
    public boolean isOn() {
        return isOn;
    }
}
