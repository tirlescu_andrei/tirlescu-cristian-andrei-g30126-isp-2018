package g30126.tirlescu.cristian.andrei.l10.e3;

public class SecondCounter extends Thread{

    private int valMin;
    private int valMax;

    public SecondCounter(int valMin,int valMax){
        this.valMin = valMin;
        this.valMax = valMax;
    }

    public void run(){
        for(int i=valMin; i<=valMax; i++){
            System.out.println("i="+i);
            try{
                Thread.sleep(100);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("Ending of SecontCounter!");
    }

}
