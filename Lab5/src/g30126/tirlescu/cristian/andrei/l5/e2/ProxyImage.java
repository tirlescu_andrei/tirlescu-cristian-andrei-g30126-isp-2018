package g30126.tirlescu.cristian.andrei.l5.e2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean directionCall;

    public ProxyImage(String fileName,boolean directionCall){
        this.fileName = fileName;
        this.directionCall = directionCall;
    }

    public boolean isDirectionCall(){
        return directionCall;
    }

    @Override
    public void display(){
        if(isDirectionCall()){
            if(realImage == null){
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else {
            if(rotatedImage == null){
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }
}
