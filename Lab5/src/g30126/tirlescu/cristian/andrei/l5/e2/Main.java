package g30126.tirlescu.cristian.andrei.l5.e2;

public class Main {
    public static void main(String[] args) {
        //directionCall = true -> RealImage display method
        //directionCall = false -> RotatedImage display method
        ProxyImage proxyImage = new ProxyImage("picture.img",true);
        proxyImage.display();
        ProxyImage proxyImage1 = new ProxyImage("picture1.img",false);
        proxyImage1.display();
    }
}
