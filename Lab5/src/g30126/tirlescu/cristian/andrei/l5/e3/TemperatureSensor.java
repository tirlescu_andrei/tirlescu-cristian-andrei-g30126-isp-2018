package g30126.tirlescu.cristian.andrei.l5.e3;

public class TemperatureSensor extends Sensor {
    public int tempSensor;

    public TemperatureSensor(){
        tempSensor = (int)(Math.random()*100);
    }

    @Override
    public int readValue() {
        return tempSensor;
    }
}
