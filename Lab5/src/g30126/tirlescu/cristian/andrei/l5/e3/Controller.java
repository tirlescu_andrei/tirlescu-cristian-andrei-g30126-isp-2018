package g30126.tirlescu.cristian.andrei.l5.e3;

public class Controller {
    private TemperatureSensor[] temperatureSensor = new TemperatureSensor[20];
    private LightSensor[] lightSensor = new LightSensor[20];

    public Controller(){
    }

    public void control(){
        for(int i=0;i<20;i++){
            temperatureSensor[i] = new TemperatureSensor();
            lightSensor[i] = new LightSensor();
            System.out.println("Temperature at "+(i+1)+".sec="+temperatureSensor[i].readValue());
            System.out.println("Light at "+(i+1)+".sec="+lightSensor[i].readValue());
        }
    }
}
