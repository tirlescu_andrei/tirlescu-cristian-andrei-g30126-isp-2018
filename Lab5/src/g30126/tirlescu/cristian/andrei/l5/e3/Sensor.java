package g30126.tirlescu.cristian.andrei.l5.e3;

abstract class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation() {
        return "Sensor location is:"+location;
    }
}
