package g30126.tirlescu.cristian.andrei.l5.e3;

public class LightSensor extends Sensor{
    public int lightSensor;

    public LightSensor(){
        lightSensor = (int)(Math.random()*100);
    }

    @Override
    public int readValue() {
        return lightSensor;
    }
}
