package g30126.tirlescu.cristian.andrei.l5.e4;
import g30126.tirlescu.cristian.andrei.l5.e3.LightSensor;
import g30126.tirlescu.cristian.andrei.l5.e3.TemperatureSensor;

public enum ControllerSingleton {
    CONTROLLER_SINGLETON;
    private TemperatureSensor[] temperatureSensor = new TemperatureSensor[20];
    private LightSensor[] lightSensor = new LightSensor[20];

    public void control() {
        for (int i = 0; i < 20; i++) {
            temperatureSensor[i] = new TemperatureSensor();
            lightSensor[i] = new LightSensor();
            System.out.println("Temperature at " + (i + 1) + "s=" + temperatureSensor[i].readValue());
            System.out.println("Light at " + (i + 1) + "s=" + lightSensor[i].readValue());
        }
    }
}
