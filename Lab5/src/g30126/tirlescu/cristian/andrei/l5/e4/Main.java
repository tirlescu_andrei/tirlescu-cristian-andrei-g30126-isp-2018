package g30126.tirlescu.cristian.andrei.l5.e4;

public class Main {
    public static void main(String[] args) {
//        ControllerSingleton controllerSingleton = new ControllerSingleton(); --Error
        ControllerSingleton.CONTROLLER_SINGLETON.control();
    }
}
