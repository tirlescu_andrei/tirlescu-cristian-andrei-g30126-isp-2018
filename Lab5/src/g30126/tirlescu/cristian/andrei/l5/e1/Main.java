package g30126.tirlescu.cristian.andrei.l5.e1;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(1,"red",true);
        shapes[1] = new Rectangle(1,2,"blue",false);
        shapes[2] = new Square(2,"yellow",false);

        for(int i=0;i<shapes.length;i++){
            System.out.println("Area["+(i+1)+"]="+shapes[i].getArea());
        }

        for(int i=0;i<shapes.length;i++){
            System.out.println("Perimeter["+(i+1)+"]="+shapes[i].getPerimeter());
        }
    }
}
