import org.junit.Test;
import tirlescu.cristian.andrei.test3.*;

import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void testRead(){
        Book book = new Book(1,"location");
        assertEquals("Sensor value is:1",book.read());
    }
}
