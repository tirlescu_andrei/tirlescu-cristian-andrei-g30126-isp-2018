package g30126.tirlescu.cristian.andrei.l6.e5;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DrawingBoard b1 = new DrawingBoard();

        System.out.println("Enter nr. of bricks");
        int nrOfBricks = scanner.nextInt();
        System.out.println("Enter positionX");
        int positionX = scanner.nextInt();
        System.out.println("Enter positionY");
        int positionY = scanner.nextInt();
        Pyramid pyramid = new Pyramid(20,10,Color.RED,positionX,positionY);

        int i = 1;//nivelul piramidei
        int k = 0;//folosit la situarea pe axa Ox
        while(nrOfBricks != 0){
            if(nrOfBricks - i >= 0){
                for(int j=1;j<=i;j++){ //j-ul folosit pentru nr de caramizi de pe linie
                    if(i==j || i>=2){ //de la nivelul 2 trebuie sa scad cu 10 lungimea randului si sa cresc cu 20 de la a doua caramida
                        k+=10;
                    }
                    b1.addPyramid(new Pyramid(20,10,Color.RED,pyramid.getPositionX()+((j-i)*10+k),pyramid.getPositionY()+((i-1)*10)));
                }
                k=0;
                nrOfBricks -= i;
                i++;
            }
        }
    }
}
