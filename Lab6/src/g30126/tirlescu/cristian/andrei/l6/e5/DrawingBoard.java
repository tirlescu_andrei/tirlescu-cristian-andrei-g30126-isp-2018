package g30126.tirlescu.cristian.andrei.l6.e5;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard extends JFrame {
    private ArrayList<Pyramid> pyramids = new ArrayList<>();

    public DrawingBoard(){
        this.setTitle("Pyramid");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(500,600);
        this.setVisible(true);
    }

    public boolean addPyramid(Pyramid pyramid){
        if(!pyramids.contains(pyramid)){
            pyramids.add(pyramid);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){
        //Error
//        for(Pyramid pyramid : pyramids){
//            pyramid.draw(g);
//        }
        for(int i=0;i<pyramids.size();i++){
            pyramids.get(i).draw(g);
        }
    }
}
