package g30126.tirlescu.cristian.andrei.l6.e5;

import java.awt.*;

public class Pyramid {
    private int width;
    private int high;
    private Color color;
    private int positionX;
    private int positionY;


    public Pyramid(int width, int high, Color color, int positionX,int positionY) {
        this.width = width;
        this.high = high;
        this.color = color;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public Color getColor() {
        return color;
    }
    public int getHigh() {
        return high;
    }
    public int getWidth() {
        return width;
    }

    public void draw(Graphics g){
        System.out.println("Drawing...");
        g.setColor(getColor());
        g.drawRect(positionX,positionY,width, high);
    }

    public int getPositionX() {
        return positionX;
    }
    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }
    public int getPositionY() {
        return positionY;
    }
    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public void setHigh(int high) {
        this.high = high;
    }
}
