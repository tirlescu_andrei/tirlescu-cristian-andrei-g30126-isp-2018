package g30126.tirlescu.cristian.andrei.l6.e3;

import java.awt.*;

public interface Shape {
    void draw(Graphics g);
}