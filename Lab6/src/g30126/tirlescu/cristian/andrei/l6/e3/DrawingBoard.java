package g30126.tirlescu.cristian.andrei.l6.e3;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    private ArrayList<Rectangle> rectangles = new ArrayList<>();
    private ArrayList<Circle> circles = new ArrayList<>();

    public DrawingBoard() {
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public boolean addRectangle(Rectangle rectangle){
        if(!rectangles.contains(rectangle)){
            rectangles.add(rectangle);
            this.repaint();
            return true;
        }
        return false;
    }

    public boolean addCircle(Circle circle){
        if(!circles.contains(circle)){
            circles.add(circle);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){
        for(Rectangle rectangle : rectangles)
            rectangle.draw(g);
        for(Circle circle : circles)
            circle.draw(g);
    }

    public boolean deleteRectangle(String id){
        for(int i = 0; i< rectangles.size(); i++){
            if(rectangles.get(i).getId().equals(id)){
                System.out.println("Rectangle with id:"+ rectangles.get(i).getId()+" was deleted!");
                rectangles.remove(i);
                return true;
            }
        }
        System.out.println("The id does not exists!");
        return false;
    }

    public boolean deleteCircle(String id){
        for(int i = 0; i< circles.size(); i++){
            if(circles.get(i).getId().equals(id)){
                System.out.println("Circle with id:"+ circles.get(i).getId()+" was deleted");
                circles.remove(i);
                return true;
            }
        }
        System.out.println("The id does not exists!");
        return false;
    }
}