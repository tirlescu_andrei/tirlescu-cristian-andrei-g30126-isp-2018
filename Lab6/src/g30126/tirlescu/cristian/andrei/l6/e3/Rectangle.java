package g30126.tirlescu.cristian.andrei.l6.e3;

import java.awt.*;

public class Rectangle implements Shape {
    private Color color;
    private int x;
    private int y;
    private final String id;
    private boolean filled;
    private int length;
    private int width;

    public Rectangle(Color color, int x, int y, String id, boolean filled,int length,int width) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
        this.length = length;
        this.width = width;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }


    public String getId() {
        return id;
    }

    public boolean isFilled() {
        return filled;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+width+"-" +length);
        g.setColor(getColor());
        g.drawRect(getX(),getY(),width,length);
        if(isFilled()){
            g.fill3DRect(getX(),getY(),width,length,true);
        }
    }
}
