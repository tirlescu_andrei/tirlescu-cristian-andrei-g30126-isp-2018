package g30126.tirlescu.cristian.andrei.l6.e4;

public class Main {
    public static void main(String[] args) {
        CharSeq charSeq = new CharSeq("Andrei");
        System.out.println(charSeq.length());
        System.out.println(charSeq.charAt(1));
        System.out.println(charSeq.subSequence(3,6));
    }
}
