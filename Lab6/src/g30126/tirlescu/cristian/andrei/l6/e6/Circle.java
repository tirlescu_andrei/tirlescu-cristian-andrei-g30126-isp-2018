package g30126.tirlescu.cristian.andrei.l6.e6;

import java.awt.*;

public class Circle {
    private int radius;
    private Color color;
    private int x;
    private int y;


    public Circle(int radius, Color color, int x, int y) {
        this.radius = radius;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }
    public Color getColor() {
        return color;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public void draw(Graphics g){
        System.out.println("Drawing...");
        g.setColor(getColor());
        x = x-(radius/2);
        y = y-(radius/2);
        g.drawOval(x,y,radius,radius);
    }
}
