package g30126.tirlescu.cristian.andrei.l6.e4arrayList;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Character> characters = new ArrayList<>();
        characters.add('A');
        characters.add('n');
        characters.add('d');
        characters.add('r');
        characters.add('e');
        characters.add('i');
        charSeq charSeq = new charSeq(characters);

        System.out.println(charSeq.length());
        System.out.println("Index:"+charSeq.charAt(1));
        //Error, nu mi-am dat seama inca de la ce
        System.out.println("Sub:"+charSeq.subSequence(2,4));
    }
}
