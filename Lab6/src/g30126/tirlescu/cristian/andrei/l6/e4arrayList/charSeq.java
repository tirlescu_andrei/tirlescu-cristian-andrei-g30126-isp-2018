package g30126.tirlescu.cristian.andrei.l6.e4arrayList;

import java.util.ArrayList;

public class charSeq implements CharSequence {

    private ArrayList<Character> characters;

    public charSeq(ArrayList<Character> characters) {
        this.characters = characters;
    }

    @Override
    public int length() {
        return characters.size();
    }

    @Override
    public char charAt(int index) {
        return characters.get(index-1);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        ArrayList<Character> newChar = new ArrayList<>();
        for (int i = start; i < end; i++) {
            newChar.add(characters.get(i));
        }
        return new charSeq(newChar);
    }
}
