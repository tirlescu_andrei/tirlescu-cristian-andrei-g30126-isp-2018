package g30126.tirlescu.cristian.andrei.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color,int x,int y,String id,boolean filled, int length,int width) {
        super(color,x,y,id,filled);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+width+"-" +length);
        g.setColor(getColor());
        g.drawRect(getX(),getY(),width,length);
        if(isFilled()){
            g.fill3DRect(getX(),getY(),width,length,true);
        }
    }
}