package g30126.tirlescu.cristian.andrei.l6.e1;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    private ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public boolean addShape(Shape shape){
        if(!shapes.contains(shape)){
            shapes.add(shape);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){
        for(Shape shape : shapes)
            shape.draw(g);
    }

    public boolean deleteShape(String id){
        for(int i=0;i<shapes.size();i++){
            if(shapes.get(i).getId().equals(id)){
                System.out.println("Shape with id:"+shapes.get(i).getId()+" was deleted");
                shapes.remove(i);
                return true;
            }
        }
        System.out.println("The id does not exists!");
        return false;
    }
}
