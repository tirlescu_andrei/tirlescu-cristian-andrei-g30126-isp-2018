package g30126.tirlescu.cristian.andrei.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    private final String id;
    private boolean filled;

    public Shape(Color color,int x,int y,String id,boolean filled) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public abstract void draw(Graphics g);

    public String getId() {
        return id;
    }

    public boolean isFilled() {
        return filled;
    }
}