package g30126.tirlescu.cristian.andrei.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,"circle1",true,50);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,50,"circle2",false,50);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,100, 100,"rectangle",false,50,100);
        b1.addShape(s3);
        b1.deleteShape("");
        b1.deleteShape("circle2");
    }
}