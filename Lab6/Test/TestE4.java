import g30126.tirlescu.cristian.andrei.l6.e4.CharSeq;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestE4 {
    @Test
    public void shouldReturnLength(){
        CharSeq charSeq = new CharSeq("Andrei");
        assertEquals(6,charSeq.length());
    }

    @Test
    public void shouldReturnCharAt(){
        CharSeq charSeq = new CharSeq("Andrei");
        assertEquals('A',charSeq.charAt(1));
    }

    @Test
    public void shouldReturnSubSequence(){
        CharSeq charSeq = new CharSeq("Andrei");
        assertEquals("rei",charSeq.subSequence(3,6));
    }
}
