import g30126.tirlescu.andrei.l3.e6.MyPoint;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestMyClass {

    MyPoint point = new MyPoint();

    @Test
    public void testXY(){
        point.setXY(1,2);
        assertEquals(1,point.getX());
        assertEquals(2,point.getY());

    }

    @Test
    public void testToString(){
        point.setXY(1,2);
        assertEquals("(1,2)",point.toString());
    }

    @Test
    public void testDistance1and2(){
        point.setXY(1,2);
        int distance = (int)(point.distance(3,4));
        assertEquals(2,distance);
        distance = (int)point.distance(new MyPoint(3,4));
        assertEquals(2,distance);
    }
}
