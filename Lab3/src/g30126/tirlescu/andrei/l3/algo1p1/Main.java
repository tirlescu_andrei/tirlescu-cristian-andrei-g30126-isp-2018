package g30126.tirlescu.andrei.l3.algo1p1;

public class Main {

    public static int solution(int[] A) {
        int result = 0;
        for(int i:A){ // i goes to A[0] to A[7]
            result ^=i; //xor between result and each element of the vector A
        }
        return result;
    }

    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);

        if (result == 7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
}
