package g30126.tirlescu.andrei.l3.e6;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint point = new MyPoint();
        point.setXY(1,2);
        System.out.println("point:"+point.toString());
        System.out.println("Distance 1 is :"+point.distance(3,4));
        System.out.println("Distance 2 is :"+point.distance(new MyPoint(3,4)));
    }
}
