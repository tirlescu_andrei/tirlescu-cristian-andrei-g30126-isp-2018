package g30126.tirlescu.andrei.l3.e6;

public class MyPoint {
    private int x;
    private int y;

    //constructor that construct a point at (0,0);
    public MyPoint() {
        x = 0; y = 0;
    }
    //constructor with two coordinates x,y
    public MyPoint(int x,int y){
        this.x = x;
        this.y = y;
    }
    //Getter and setter
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y){
        setX(x);
        setY(y);
    }

    public String toString() {
        return "("+x+","+y+")";
    }

    public double distance(int x,int y){
        return (Math.sqrt(Math.pow((x-this.x),2) + Math.pow(y-this.y,2)));
    }

    //Overloaded
    public double distance(MyPoint myPoint){
        return (Math.sqrt(Math.pow(myPoint.x-this.x,2) + Math.pow(myPoint.y-this.y,2)));
    }
}
