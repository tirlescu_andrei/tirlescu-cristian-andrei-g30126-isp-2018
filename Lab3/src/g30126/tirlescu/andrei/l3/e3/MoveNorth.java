package g30126.tirlescu.andrei.l3.e3;
import becker.robots.*;

public class MoveNorth {
    public static void main(String[] args) {
        City Cluj = new City();
        Robot Andrei = new Robot(Cluj,1,1,Direction.NORTH);
        // r1 makes 5 moves to north
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        // r1 turns around
        Andrei.turnLeft();
        Andrei.turnLeft();
        // r1 returns to its starting point
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.move();
    }
}
