package g30126.tirlescu.andrei.l3.e5;
import becker.robots.*;


public class FetchingTheNewspaper {
    public static void main(String[] args) {
        City Cluj = new City();
        Thing newspaper = new Thing(Cluj,2,2);
        Robot Andrei = new Robot(Cluj,1,2,Direction.SOUTH);
        Wall blockAve0 = new Wall(Cluj,1,2,Direction.NORTH);
        Wall blockAve1 = new Wall(Cluj,1,1,Direction.NORTH);
        Wall blockAve2 = new Wall(Cluj,1,2,Direction.EAST);
        Wall blockAve3 = new Wall(Cluj,1,2,Direction.SOUTH);
        Wall blockAve4 = new Wall(Cluj,1,1,Direction.WEST);
        Wall blockAve5 = new Wall(Cluj,2,1,Direction.WEST);
        Wall blockAve6 = new Wall(Cluj,2,1,Direction.SOUTH);
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.pickThing();

        //The robot picked the newspaper

        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.putThing();
        Andrei.turnLeft();
        Andrei.turnLeft();
        Andrei.turnLeft();
    }
}
