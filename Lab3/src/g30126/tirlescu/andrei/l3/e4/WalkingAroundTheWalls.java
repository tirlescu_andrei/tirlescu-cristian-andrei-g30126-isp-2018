package g30126.tirlescu.andrei.l3.e4;
import becker.robots.*;

public class WalkingAroundTheWalls {
    public static void main(String[] args) {
        City Cluj = new City();
        Wall blockAve0= new Wall(Cluj,1,1,Direction.NORTH);
        Wall blockAve1= new Wall(Cluj,1,2,Direction.NORTH);
        Wall blockAve2= new Wall(Cluj,1,1,Direction.WEST);
        Wall blockAve3= new Wall(Cluj,2,1,Direction.WEST);
        Wall blockAve4= new Wall(Cluj,2,1,Direction.SOUTH);
        Wall blockAve5= new Wall(Cluj,2,2,Direction.SOUTH);
        Wall blockAve6= new Wall(Cluj,1,2,Direction.EAST);
        Wall blockAve7= new Wall(Cluj,2,2,Direction.EAST);
        Robot Andrei = new Robot(Cluj,0,2,Direction.WEST);
        Andrei.move();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
        Andrei.move();
        Andrei.move();
        Andrei.turnLeft();
        Andrei.move();
    }
}
