package g30126.tirlescu.cristian.andrei.l8.e2;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Andrei",100);
        bank.addAccount("Maria",50);
        bank.addAccount("Laura",300);
        bank.addAccount("Claudiu",1000);
        bank.addAccount("Cristian",10);
        bank.addAccount("Raul",126);

        bank.printAccounts();
        System.out.println("============");
        bank.printAccounts(50,300);
        System.out.println("============");
        bank.getAllAccounts();
    }

}


