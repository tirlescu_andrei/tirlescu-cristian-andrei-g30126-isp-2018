package g30126.tirlescu.cristian.andrei.l8.e2;

import g30126.tirlescu.cristian.andrei.l8.e1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public boolean addAccount(String owner, double balance){
        BankAccount bankAccount = new BankAccount(owner,balance);
        if(!accounts.contains(bankAccount)){
            accounts.add(bankAccount);
            return true;
        }
        return false;
    }

    public void printAccounts(){
        Collections.sort(accounts); //Daca folosin tree set nu mai trebuie ca asta face tree set
        print();
    }

    public void printAccounts(double minBalance,double maxBalance){
//        Collections.sort(accounts);
        BankAccount bMin = findAccount(minBalance);
        BankAccount bMax = findAccount(maxBalance);
        int index = accounts.indexOf(bMin);

        while (bMin.getBalance() <= bMax.getBalance()){
            System.out.println("Owner:" +bMin.getOwner()+ " balance:" +bMin.getBalance());
            index++;
            bMin = accounts.get(index);
        }
    }

    public BankAccount findAccount(double amount){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getBalance() == amount)
                return accounts.get(i);
        }
        return null;
    }

    public BankAccount getAccount(String owner){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getOwner().equals(owner))
                return accounts.get(i);
        }
        return null;
    }


    public void getAllAccounts(){
        Collections.sort(accounts,ownerComparator);
        print();
    }

    public void print(){
        for(int i=0; i<accounts.size(); i++){
            System.out.println("Owner:" +accounts.get(i).getOwner() + " balance:" +accounts.get(i).getBalance());
        }
    }

    public static Comparator<BankAccount> ownerComparator = (o1, o2) -> {
        String owner1 = o1.getOwner().toUpperCase();
        String owner2 = o2.getOwner().toUpperCase();

        return owner1.compareTo(owner2);
    };


}
