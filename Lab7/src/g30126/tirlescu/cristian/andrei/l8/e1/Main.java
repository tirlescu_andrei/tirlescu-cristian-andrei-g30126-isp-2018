package g30126.tirlescu.cristian.andrei.l8.e1;

public class Main {
    public static void main(String[] args) {
        BankAccount account1 = new BankAccount("Andrei",100);
        BankAccount account2 = new BankAccount("Andrei",100);
        BankAccount account3 = new BankAccount("Maria",500);
        account1.withdraw(50);
        account2.withdraw(50);

        //return true
        if(account1.equals(account2)){
            System.out.println("Accounts are equals!");
        }
        else
            System.out.println("Accounts are not equals!");

        //return false
        if(account1.equals(account3)){
            System.out.println("Accounts are equals!");
        }
        else
            System.out.println("Accounts are not equals!");

        account1.deposit(100);
        account2.deposit(100);

        //return true
        if(account1.hashCode() == account2.hashCode()){
            System.out.println("HashCode equals!");
        }
        else{
            System.out.println("HashCode not equals!");
        }

        //return false
        if(account1.hashCode() == account3.hashCode()){
            System.out.println("HashCode equals!");
        }
        else{
            System.out.println("HashCode not equals!");
        }

    }
}
