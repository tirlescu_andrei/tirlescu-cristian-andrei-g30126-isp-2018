package g30126.tirlescu.cristian.andrei.l8.e1;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        if (this.balance - amount > 0) {
            this.balance -= amount;
        } else {
            System.out.println("Insufficient amount!");
        }
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public int compareTo(BankAccount bankAccount) {
        return (int)(balance - bankAccount.balance);
    }

}
