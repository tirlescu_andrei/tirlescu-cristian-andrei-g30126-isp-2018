package g30126.tirlescu.cristian.andrei.l8.e3;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Andrei",505.25);
        bank.addAccount("Claudiu",105.21);
        bank.addAccount("Raul",102.52);
        bank.addAccount("Bobu",2054);
        bank.addAccount("Miruna",542);
        bank.addAccount("Aurel",203);
        bank.addAccount("Liviu",684);

        bank.printAccounts(500,700);
        System.out.println("=======");
        System.out.println(bank.getAccount("Andrei",505.25));
        System.out.println(bank.getAccount("Maria",505.25));
    }
}
