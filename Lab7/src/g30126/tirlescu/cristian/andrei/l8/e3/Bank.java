package g30126.tirlescu.cristian.andrei.l8.e3;

import java.util.TreeSet;

public class Bank {
    TreeSet t = new TreeSet();

    public void addAccount(String owner, double balance){
        BankAccount bankAccount = new BankAccount(owner,balance);
        t.add(bankAccount);
    }

    public void printAccounts(){
        System.out.println("Printing accounts...");
        System.out.println(t);
    }

    public void printAccounts(double minBalance, double maxBalance){
        System.out.println("Printing accounts between " +minBalance + " and " +maxBalance);
        System.out.println(t.subSet(new BankAccount("x",minBalance),new BankAccount("y",maxBalance)));
    }

    public BankAccount getAccount(String owner, double amount){
        BankAccount bankAccount = new BankAccount(owner,amount);
        if(t.contains(bankAccount)){
            return bankAccount;
        }
        return null;
    }
}
