package g30126.tirlescu.cristian.andrei.l8.e3;

public class BankAccount implements Comparable {

    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    @Override
    public int compareTo(Object o) {
        BankAccount bankAccount = (BankAccount)o;
        if(balance > bankAccount.balance) return 1;
        if(balance == bankAccount.balance) return 0;
        return -1;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
