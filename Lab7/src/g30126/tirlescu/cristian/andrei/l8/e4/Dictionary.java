package g30126.tirlescu.cristian.andrei.l8.e4;
import java.util.HashMap;
import java.util.Iterator;

public class Dictionary {
    private HashMap hashMap = new HashMap();

    public Dictionary(){}

    public void addWord(Word w,Definition d){
        if(!hashMap.containsKey(w)){
            System.out.println("Adding word!");
            hashMap.put(w,d);
        }
        else
        {
            System.out.println("The word already exists, updating...");
            hashMap.put(w,d);
        }
    }

    public Definition getDefinition(Word w){
        if(hashMap.containsKey(w)){
            Definition definition = (Definition) hashMap.get(w);
            return definition;
        }
        return null;
    }

    public void getAllWords(){
        System.out.println("\nGetting all the words ...\n");
        Iterator iterator = hashMap.keySet().iterator();
        while(iterator.hasNext())
            System.out.println(iterator.next());
    }

    public void getAllDefinition(){
        System.out.println("\nGetting all the definitions ...\n");
        for (Word word : (Iterable<Word>) hashMap.keySet()) {
            Definition def = getDefinition(word);
            System.out.println(def);
        }
    }
}
