package g30126.tirlescu.cristian.andrei.l8.e4;

public class Main{
    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary();
        HashMap hashMap = new HashMap(dictionary);
        dictionary.addWord(new Word("Car"),new Definition("An mechanical automobile"));
        dictionary.addWord(new Word("PC"),new Definition("A personal computer"));
        dictionary.addWord(new Word("Water"),new Definition("A liquid without smell and taste"));
        dictionary.getAllWords();
        dictionary.getAllDefinition();
    }
}
