package g30126.tirlescu.andrei.l2.e3;
import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("A=");
        int A = in.nextInt();
        System.out.println("B=");
        int B = in.nextInt();
        int primeNumbers=0;
        System.out.println("Printing prime number");
        for(int number=A;number<=B;number++){
            if(isPrime(number)){
                System.out.print(number+" ");
                primeNumbers++;
            }
        }
        System.out.println("\n"+"There are "+primeNumbers+" prime numbers");
    }

    static boolean isPrime(int number){
        for(int i=2; i<number; i++){
            if(number%i == 0){
                return false;
            }
        }
        return  true;
    }
}
