package g30126.tirlescu.andrei.l2.e2;
import java.util.Scanner;

public class PrintNumberInWord {

    static void nestedIF(int testscore) {
        String numberInLetter;

        if (testscore == 1) {
            numberInLetter = "ONE";
        } else if (testscore == 2) {
            numberInLetter = "TWO";
        } else if (testscore == 3) {
            numberInLetter = "THREE";
        } else if (testscore == 4) {
            numberInLetter = "FOUR";
        } else if (testscore == 5) {
            numberInLetter = "FIVE";
        } else if (testscore == 6) {
            numberInLetter = "SIX";
        } else if (testscore == 7) {
            numberInLetter = "SEVEN";
        } else if (testscore == 8) {
            numberInLetter = "EIGHT";
        } else if (testscore == 9) {
            numberInLetter = "NINE";
        } else {
            numberInLetter = "OTHER";
        }
        System.out.println("Number = " + numberInLetter);
    }

    static void switchCase(int testscore){
        String numberInLetter;
        switch (testscore) {
            case 1:  numberInLetter = "ONE";
                break;
            case 2:  numberInLetter = "TWO";
                break;
            case 3:  numberInLetter = "THREE";
                break;
            case 4:  numberInLetter = "FOUR";
                break;
            case 5:  numberInLetter = "FIVE";
                break;
            case 6:  numberInLetter = "SIX";
                break;
            case 7:  numberInLetter = "SEVEN";
                break;
            case 8:  numberInLetter = "EIGHT";
                break;
            case 9:  numberInLetter = "NINE";
                break;
            default: numberInLetter = "OTHER";
                break;
        }
        System.out.println("Number = "+numberInLetter);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int testscore = in.nextInt();
        System.out.println("Nested-If");
        nestedIF(testscore);
        System.out.println("Switch-Case");
        switchCase(testscore);
    }
}