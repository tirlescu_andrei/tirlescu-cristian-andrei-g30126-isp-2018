package g30126.tirlescu.andrei.l2.e5;
import java.util.Random;

public class BubbleSort {
    static int v[];
    public static void main(String[] args){
        Random r = new Random();
        v = new int[10];
        for(int i=0;i<10;i++){
            v[i] = r.nextInt(50);
        }
        System.out.println("Random elements:");
        printArray();
        System.out.print("\n");
        bubbleSort();
        System.out.println("After bubble sort");
        printArray();
    }

    static void printArray(){
        for(int i=0;i<10;i++){
            System.out.print(" "+v[i]);
        }
    }

    static void bubbleSort(){
        for(int i=0;i<9;i++){
            for(int j=0;j<9-i;j++){
                if(v[j] > v[j+1]){
                    int temp;
                    temp=v[j];
                    v[j]=v[j+1];
                    v[j+1]=temp;
                }
            }
        }
    }
}


