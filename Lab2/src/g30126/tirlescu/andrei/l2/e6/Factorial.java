package g30126.tirlescu.andrei.l2.e6;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("n=");
        int n = in.nextInt();
        System.out.println(+n+"! using recursive method = "+factorialRecursiv(n));
        System.out.println(+n+"! using nerecursive method = "+factorialNerecursiv(n));
    }

    static int factorialRecursiv(int n){
        int result;
        if(n==1)
            return 1;
        result = factorialRecursiv(n-1) * n;
        return result;
    }

    static int factorialNerecursiv(int n){
        int j=1;
        for(int i=1;i<=n;i++){
            j *=i;
        }
        return j;
    }
}
