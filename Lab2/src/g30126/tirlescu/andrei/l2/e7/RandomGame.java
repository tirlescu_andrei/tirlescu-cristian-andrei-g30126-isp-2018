package g30126.tirlescu.andrei.l2.e7;
import java.util.Scanner;
import java.util.Random;

public class RandomGame {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Random r = new Random();
        int random = r.nextInt(10);
        System.out.println("Random number is: "+random);
        for(int i=0;i<3;i++){
            System.out.println("Enter the guess number :");
            int guess = in.nextInt();
            if(guess == random) {
                System.out.println("You guessed the number");
                return;
            } else if(guess < random){
                System.out.println("'Wrong answer, your number is too low");
            } else System.out.println("Wrong answer, your number it too high");
        }
    }
}
