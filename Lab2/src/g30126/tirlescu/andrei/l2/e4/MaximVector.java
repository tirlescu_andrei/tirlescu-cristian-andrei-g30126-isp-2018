package g30126.tirlescu.andrei.l2.e4;
import java.util.Scanner;
import java.util.Random;

public class MaximVector {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Random r = new Random();
        System.out.println("n=");
        int n = in.nextInt();
        int v[] = new int[n];

        for(int i=0;i<n;i++){
            v[i] = r.nextInt(n);
        }

        System.out.println("Random elements:");
        for(int i=0;i<n;i++){
            System.out.print(" "+v[i]);
        }

        System.out.print("\n");
        int maxim = v[0];
        for(int i=1;i<n;i++){
            if(v[i] > maxim){
                maxim = v[i];
            }
        }
        System.out.println("The maxim element of vertor is: "+maxim);
    }
}
