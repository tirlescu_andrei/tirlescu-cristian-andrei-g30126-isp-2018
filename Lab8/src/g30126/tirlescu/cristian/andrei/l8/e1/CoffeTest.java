package g30126.tirlescu.cristian.andrei.l8.e1;

class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
            try {
                Coffee c = mk.makeCoffee();
                d.drinkCoffee(c);
            } catch (MaximumException e){
                System.out.println("Exception:"+e.getMessage());
                return;
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the coffee cup.\n");
            }
        }

    }
}

class CoffeeMaker {
    private int nrCoffee;
    private static final int MAX = 5;
    CoffeeMaker(){nrCoffee = 0;}
    Coffee makeCoffee() throws MaximumException{
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        if(nrCoffee < MAX){
            nrCoffee ++;
        } else {
            throw new MaximumException("It cannot make any coffee!");
        }
        return coffee;
    }
}

class Coffee{
    private int temp;
    private int conc;

    Coffee(int t,int c){temp = t;conc = c;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+"]";}
}

class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
        System.out.println("Drink coffee:"+c);
    }
}

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}

class ConcentrationException extends Exception {
    int c;

    public ConcentrationException(int c, String msg) {
        super(msg);
        this.c = c;
    }

    int getConc() {
        return c;
    }
}

class MaximumException extends Exception{
    public MaximumException(String msg){
        super(msg);
    }
}