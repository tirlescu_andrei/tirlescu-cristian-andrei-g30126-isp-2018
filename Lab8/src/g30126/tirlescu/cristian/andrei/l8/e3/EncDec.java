package g30126.tirlescu.cristian.andrei.l8.e3;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class EncDec {
    public static Scanner scanner = new Scanner(System.in);
    public static ArrayList<Character> charactersEnc = new ArrayList<>();
    public static ArrayList<Character> charactersDec = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        System.out.print("Enter the namefile for encrypting: ");
        String fileEnc = scanner.nextLine();
        System.out.print("\nEnter the namefile for decrypting: ");
        String fileDec = scanner.nextLine();

        FileWriter enc = new FileWriter("C:\\Users\\Andrei\\IdeaProjects\\Lab8\\src\\g30126\\tirlescu\\cristian\\andrei\\l8\\e3\\"+fileEnc);
        FileWriter dec = new FileWriter("C:\\Users\\Andrei\\IdeaProjects\\Lab8\\src\\g30126\\tirlescu\\cristian\\andrei\\l8\\e3\\"+fileDec);



        try{
            encrypt(enc);
            decrypt(dec);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

    }

    static void encrypt(FileWriter enc) throws IOException{
        BufferedReader in = new BufferedReader(
                new FileReader("C:\\Users\\Andrei\\IdeaProjects\\Lab8\\src\\g30126\\tirlescu\\cristian\\andrei\\l8\\e3\\in.txt"));
        String s, s2 = new String();
        while ((s = in.readLine()) != null)
            s2 += s + "\n";
        in.close();

        for(int i=0; i<s2.length(); i++){
            int ascii = (int)s2.charAt(i)^i;
            charactersEnc.add((char)(ascii));
        }

        for (Character c : charactersEnc) {
            enc.write(c);
        }
        enc.close();
    }

    static void decrypt(FileWriter dec) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Andrei\\IdeaProjects\\Lab8\\src\\g30126\\tirlescu\\cristian\\andrei\\l8\\e3\\out.enc"));
        String s,s2 = new String();
        while((s = in.readLine()) != null)
            s2 += s + "\n";
        in.close();

        for(int i=0; i<s2.length()-1; i++){ // am scazut -1 pentru ca avem '0' la sfarsitul fisierului
            int ascii = (int)s2.charAt(i)^i;
            charactersDec.add((char)(ascii));
        }

        for(Character c : charactersDec){
            dec.write(c);
        }
        dec.close();
    }
}
