package g30126.tirlescu.cristian.andrei.l8.e2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class NrChar {
    public static Scanner scanner = new Scanner(System.in);
    public static int counter = 0;
    public static void main(String[] args) throws IOException {

        //Reading from file
        BufferedReader in = new BufferedReader(
                new FileReader("C:\\Users\\Andrei\\IdeaProjects\\Lab8\\src\\g30126\\tirlescu\\cristian\\andrei\\l8\\e2\\in.txt"));
        String s, s2 = new String();
        while((s = in.readLine())!= null)
            s2 += s + "\n";
        in.close();

        //Show the lines from files
        System.out.println(s2);

        //Show the appearance
        System.out.print("Please enter the character: ");
        char c = scanner.next().charAt(0);
        nrCaracterDat(s2,c);
        System.out.println("e appears " + counter + " times");
    }

    static void nrCaracterDat(String s,char c){
        for(int i=0;i<s.length();i++){
            if(s.charAt(i) == c){
                counter++;
            }
        }
    }
}
