package g30126.tirlescu.cristian.andrei.l8.e4;

import java.io.*;

public class CarHandler {
    public Car createCar(String name,int price){
        Car c = new Car(name,price);
        System.out.println(c+" is create");
        return c;
    }

    public void parkingCar(Car c,String storeRecipientName) throws IOException {
        ObjectOutputStream o = new ObjectOutputStream(
                new FileOutputStream(storeRecipientName));
        o.writeObject(c);
        System.out.println(c+" is parking...");
    }

    public Car getCar(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(
                new FileInputStream(storeRecipientName));
        Car c = (Car)in.readObject();
        System.out.println(c+" is getting out the garage");
        return c;
    }
}
