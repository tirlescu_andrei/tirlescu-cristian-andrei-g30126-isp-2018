package g30126.tirlescu.cristian.andrei.l8.e4;

import java.io.Serializable;

public class Car implements Serializable {
    private String model;
    private int price;

    public Car(String model, int price){
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }
}
