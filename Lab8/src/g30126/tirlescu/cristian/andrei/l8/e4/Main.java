package g30126.tirlescu.cristian.andrei.l8.e4;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        CarHandler carHandler = new CarHandler();

        Car audi = carHandler.createCar("A5",10000);
        Car mercedes = carHandler.createCar("A220",25000);


        try{
            carHandler.parkingCar(audi,"audiGarage.dat");
            carHandler.parkingCar(mercedes,"mercedesGarage.dat");

            Car x = carHandler.getCar("audiGarage.dat");
            Car y = carHandler.getCar("mercedesGarage.dat");
            System.out.println(x);
            System.out.println(y);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

}
