package g30126.tirlescu.cristian.andrei.l4.e1;

public class Conveyor {

    private Box[] packages = new Box[30];
    void addPackage(Box p, int pos){
        packages[pos] = p;
    }

    public void moveLeft(){
        for(int i=0;i<packages.length-1;i++){
            packages[i] = packages[i+1];
        }
        packages[packages.length-1] = null;

    }

    public void moveRight() {
        for (int i = packages.length - 1; i > 0; i--) {
            packages[i] = packages[i - 1];
        }
        packages[0] = null;
    }

    public Box getBox(int pos){
        return packages[pos];
    }

    public Box pickBox(int pos){
        Box temp = new Box();
        if(pos < 0 || pos >30){
            System.out.println("Invalid position");
            return null;
        }
        else if(packages[pos] != null){
            System.out.println("You picked the box at the position: "+pos);
            temp = packages[pos];
            packages[pos] = null;
            return temp;
        }
        else{
            System.out.println("The box does not exists!");
            return null;
        }
//        packages[pos] = null;
    }

    public void display(){
        int k = 0;
        for (int i = 0; i < packages.length; i++) {
            String c = (packages[i]!=null)?packages[i].toString():"_";
            System.out.print(c);
        }
        System.out.println();
    }
}
