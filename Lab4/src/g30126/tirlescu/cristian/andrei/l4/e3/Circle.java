package g30126.tirlescu.cristian.andrei.l4.e3;

import static java.lang.StrictMath.PI;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle(){
    }

    public Circle(double radius){
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return PI*Math.pow(radius,2);
    }
}
