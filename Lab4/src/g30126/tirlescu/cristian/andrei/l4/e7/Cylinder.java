package g30126.tirlescu.cristian.andrei.l4.e7;
import g30126.tirlescu.cristian.andrei.l4.e3.Circle;

public class Cylinder extends Circle{
    double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius,double height){
        super(radius);
        this.height = height;
    }

    public double getHeight(){
        return this.height;
    }

    public double getVolume(){
        return (2*getArea()+(height*2*Math.PI*getRadius()));
    }
}
