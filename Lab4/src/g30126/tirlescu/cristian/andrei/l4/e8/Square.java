package g30126.tirlescu.cristian.andrei.l4.e8;

public class Square extends Rectangle {
    public Square(){

    }

    public Square(double side){
        super(side,side);
    }

    public Square(double side,String color,boolean filled){
        super(side,side,color,filled);
    }

    public double getSide(){
        return super.getLength();
    }

    public void setSide(double side){
        super.length = super.width = side;
    }

    public void setWidth(double side){
        super.width = side;
    }

    public void setLength(double side){
        super.length = side;
    }

    @Override
    public String toString() {
        return "A Square with side="+getSide()+" which is a subclass of "+super.toString();
    }
}
