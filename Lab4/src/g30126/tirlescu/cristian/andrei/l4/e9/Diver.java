package g30126.tirlescu.cristian.andrei.l4.e9;
import becker.robots.*;

public class Diver {
    public static void main(String[] args) {
        City pool = new City();
        Wall blockAve0 = new Wall(pool,2,1,Direction.NORTH);
        Wall blockAve1 = new Wall(pool,2,0,Direction.EAST);
        Wall blockAve2 = new Wall(pool,3,0,Direction.EAST);
        Wall blockAve3 = new Wall(pool,4,0,Direction.EAST);
        Wall blockAve4 = new Wall(pool,4,1,Direction.WEST);
        Wall blockAve5 = new Wall(pool,4,1,Direction.SOUTH);
        Wall blockAve6 = new Wall(pool,4,2,Direction.SOUTH);
        Wall blockAve7 = new Wall(pool,4,2,Direction.EAST);
        Robot karel = new Robot(pool,1,1,Direction.NORTH);

        karel.move();
        moveRight(karel);
        karel.move();
        moveRight(karel);
        karel.move();
        flip(karel);
        flip(karel);
        flip(karel);
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
    }

    static void moveRight(Robot robot){
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
    }

    static void flip(Robot robot){
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
    }
}
