package g30126.tirlescu.cristian.andrei.l4.e6;

import g30126.tirlescu.cristian.andrei.l4.e4.Author;

import java.util.Arrays;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0 ;

    public Book(String name,Author[] authors,double price){
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name,Author[] authors,double price,int qtyInStock){
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName(){
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice(){
        return price;
    }
    public int getQtyInStock(){
        return qtyInStock;
    }
    public void setPrice(int price){
        this.price = price;
    }
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "book-"+getName()+" by "+authors.length+" authors";
    }

    public void printAuthors(){
        for(int i=0;i<authors.length;i++){
            System.out.println((i+1)+"# Author name"+" is "+authors[i].getName());
        }
    }
}
