package g30126.tirlescu.cristian.andrei.l4.e5;

import g30126.tirlescu.cristian.andrei.l4.e4.Author;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void shouldReturnToString(){
        Book book = new Book("book",new Author("Andrei","andreitirlescu@gmail.com",'m'),10.2);
        assertEquals("book-book Author-Andrei (m) at email: andreitirlescu@gmail.com",book.toString());
    }

    @Test
    public void shouldReturnName(){
        Book book = new Book("Thinking in Java",new Author("Andrei","andreitirlescu@gmail.com",'m'),10.2);
        assertEquals("Thinking in Java",book.getName());
    }
}
