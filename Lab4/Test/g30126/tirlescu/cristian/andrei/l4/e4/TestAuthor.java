package g30126.tirlescu.cristian.andrei.l4.e4;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestAuthor {
    @Test
    public void toSting(){
        Author author = new Author("Andrei","andreitirlescu@gmail.com",'m');
        assertEquals("Author-Andrei (m) at email: andreitirlescu@gmail.com",author.toString());
    }
}
