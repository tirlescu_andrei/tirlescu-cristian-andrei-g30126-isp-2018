package g30126.tirlescu.cristian.andrei.l4.e3;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCircle {

    @Test
    public void shouldGetAria(){
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2);
        assertEquals(Math.PI*Math.pow(circle1.getRadius(),2),circle1.getArea(),0.01);
        assertEquals(Math.PI*Math.pow(circle2.getRadius(),2),circle2.getArea(),0.01);
    }
}
