package g30126.tirlescu.cristian.andrei.l4.e8;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestShape {
    @Test
    public void shouldGetArea(){
        Circle circle = new Circle(1.0,"blue",true);
        Rectangle rectangle = new Rectangle(1.0,2.0,"green",false);
        Square square = new Square(2.0,"orange",true);
        assertEquals(Math.PI*Math.pow(circle.getRadius(),2),circle.getArea(),0.1);
        assertEquals(rectangle.getLength()*rectangle.getWidth(),rectangle.getArea(),0.1);
        assertEquals(Math.pow(square.getSide(),2),square.getArea(),0.1);
    }

    @Test
    public void shouldGetPerimeter(){
        Circle circle = new Circle(1.0,"blue",true);
        Rectangle rectangle = new Rectangle(1.0,2.0,"green",false);
        circle.setRadius(2.0);
        rectangle.setLength(2.0);
        rectangle.setWidth(3);
        assertEquals(2*Math.PI*circle.getRadius(),circle.getPerimeter(),0.1);
        assertEquals(2*(rectangle.getWidth()+rectangle.getLength()),rectangle.getPerimeter(),0.1);
    }
}
