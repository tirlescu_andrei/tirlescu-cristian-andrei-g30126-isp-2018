package g30126.tirlescu.cristian.andrei.l4.e6;

import g30126.tirlescu.cristian.andrei.l4.e4.Author;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void testBook(){
        Author[] authors = new Author[2];
        authors[0] = new Author("Andrei","email",'m');
        authors[1] =  new Author("Claudiu","email",'m');
        Book book = new Book("Luceafarul",authors,0.2);
        assertEquals("Andrei",authors[0].getName());
        assertEquals("Claudiu",authors[1].getName());
        authors[1].setEmail("claudiu@email.com");
        assertEquals("claudiu@email.com",authors[1].getEmail());
    }
}
