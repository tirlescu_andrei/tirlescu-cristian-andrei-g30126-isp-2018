package g30126.tirlescu.cristian.andrei.l4.e7;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCylinder {
    @Test
    public void shouldGetVolume(){
        Cylinder cylinder = new Cylinder(2.0,6.0);
        System.out.println("Volume="+cylinder.getVolume());
        assertEquals(2*cylinder.getArea()+(cylinder.getHeight()*2*Math.PI*cylinder.getRadius()),cylinder.getVolume(),0.1);
    }
}
